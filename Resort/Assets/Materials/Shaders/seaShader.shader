﻿Shader "Custom/seaShader"
{
    Properties
    {
        _Color1 ("BaseColor", Color) = (1,0,0,1)
		_Color2("AuxColor", Color)=(0, 0, 1, 1)
		_debug("debug", float)=1
    }
	SubShader
	{ 
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
		Blend SrcAlpha OneMinusSrcAlpha
		Pass{
			CGPROGRAM
			#pragma vertex vertexFunc
			#pragma fragment fragmentFunc

			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			struct v2f {
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0; 
				float4 screenPos : TEXCOORD1;
			};

			fixed4 _Color1;
			fixed4 _Color2;
			float _debug;

			v2f vertexFunc(appdata IN) {
				v2f OUT;
				OUT.position = UnityObjectToClipPos(IN.vertex);
				OUT.uv = IN.uv;
				OUT.screenPos = ComputeScreenPos(OUT.position);

				return OUT;
			}

			uniform sampler2D _CameraDepthTexture;

			fixed4 fragmentFunc(v2f IN) : SV_Target {

				half depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(IN.screenPos)));
				half screenDepth = _debug-IN.screenPos.w;
				fixed4 col = saturate(lerp(_Color1, _Color2, screenDepth));
				col = depth / _debug;
				return col;
			}

			ENDCG
		}
	}
}
