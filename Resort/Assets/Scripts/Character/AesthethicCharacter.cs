﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AesthethicCharacter : MonoBehaviour
{
    private Rigidbody rb;
    private AirCraft_Movement am;
    public TrailRenderer lr_R;
    public TrailRenderer lr_L;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        am = GetComponent<AirCraft_Movement>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        lr_L.startColor = new Color(1, 1, 1, 0);
        lr_L.endColor = new Color(1, 1, 1, am.getOverallSpin());
        lr_R.startColor = new Color(1, 1, 1, 0);
        lr_R.endColor = new Color(1, 1, 1, am.getOverallSpin());
    }
}
