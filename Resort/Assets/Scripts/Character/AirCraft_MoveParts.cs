﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirCraft_MoveParts : MonoBehaviour
{
    public Transform flaps;
    public float flapsMovementThreshold = 30;
    public Transform rudder;
    public float rudderMovementThreshold = 30;
    public Transform ailerons;
    private AirCraft_Movement script;

    // Start is called before the first frame update
    void Start()
    {
        script = GetComponent<AirCraft_Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        flaps.localRotation = Quaternion.AngleAxis(flapsMovementThreshold*script.PitchPow, Vector3.right);
        rudder.localRotation = Quaternion.AngleAxis(rudderMovementThreshold*-1 * script.YawPow, Vector3.up);
    }
}
