﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirCraft_Movement : MonoBehaviour
{
    [Header("Thrust")]
    public float maxVel = 650;
    [Space(4)]
    public float acc = 0;
    public float maxAcc = 10;
    public float accRate = 5.0f;
    public float deccRate = 4.5f;
    private Vector3 thrustVec;

    [Header("Brake")]
    public float brake = 0;
    public float maxBrake = 10;
    public float brakeRate = 5.0f;
    public float brakeReleaseRate = 2.0f;
    private Vector3 brakeVec;

    [Space(10)]
    [Header("Sustenation")]
    [Range(0, 5)]
    public float minSustentation = 0.0f;
    [Range(0, 5)]
    public float maxSustentation = 5.0f;
    [Range(0, 1)]
    public float stallMinSustentation = 0.1f; //Valor mínimo de sustentación en pérdida. Si esto por Vector3 up es mayor que la sustentación, la sustituye
    public float sustentationHeight = 25.0f;
    private float startY = 0;
    private float sustentationSurface = 0.5f;
    public float sustenationK = 0.05f;
    private Vector3 sustentationVec;
    //Sustentation vec magnitude that fully kills gravity without pushing you up is around 0.165

    [Space(10)]
    [Header("Orientation")]
    public float maniobrability = 1f;
    public Vector3 horDirVec;
    public Vector3 verDirVec;
    private float dirMaxLength = 1;
    [Space(3)]
    [SerializeField]
    private float yawPow = 0;    public float YawPow { get => yawPow;}
    public float maxYaw = 1;
    public float yawRotRate;
    public float yawRecoverRate;
    [Space(3)]
    [SerializeField]
    private float pitchPow = 0;    public float PitchPow { get => pitchPow;}
    public float maxPitch = 1;
    public float pitchRotRate;
    public float pitchRecoverRate;
    [Space(3)]
    [SerializeField]
    private float rollPow = 0;
    public float maxRoll = 1;
    public float rollRotRate;
    public float rollRecoverState;

    [Space(5)]
    [Range(0, 1)]
    public float velToDir = 20; //Velocidad a partir de la cual el avión es totalmente maniobrable

    private Rigidbody rb;
    [SerializeField]
    private bool onGround;
    public bool OnGround { get => onGround; set => onGround = value; }
    public float getOverallSpin()
    {
        return
        (Mathf.Abs(yawPow)+
        Mathf.Abs(rollPow)+
        Mathf.Abs(pitchPow))/3;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        horDirVec = verDirVec = new Vector3(0, 0, 0);
        startY= transform.position.y;
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //(Input.GetAxis("Horizontal")
        #region Thrust
        if (Input.GetKey(KeyCode.Space) && !Input.GetKey(KeyCode.LeftShift))
        {
            acc = Mathf.Clamp(acc + accRate * Time.deltaTime, 0, maxAcc);
        }
        else
        {
            acc = Mathf.Clamp(acc - (deccRate * Time.deltaTime), 0, maxVel);
        }
            thrustVec = transform.forward * Time.deltaTime * acc; //Steer. Velocidad que empuja hacia delante
        #endregion

        #region Brake
        if(Input.GetKey(KeyCode.Space) && Input.GetKey(KeyCode.LeftShift))
        {
            brake = Mathf.Clamp(brake + brakeRate * Time.deltaTime, 0, maxBrake);
        }
        else
        {
            brake = Mathf.Clamp(brake - brakeReleaseRate * Time.deltaTime, 0, maxBrake);
        }
        brakeVec = -transform.forward * Time.deltaTime * brake * Mathf.Sqrt((1-Mathf.Abs(Vector3.Dot(rb.velocity.normalized, Vector3.up))));
        brake *= (1 - Mathf.Abs(Vector3.Dot(rb.velocity.normalized, Vector3.up)));
        #endregion

        #region Sustentation
        //Primero calculo la sustentación con la formula
        sustentationVec = (sustentationHeight - Mathf.Clamp(transform.position.y-startY, 0, sustentationHeight)) / sustentationHeight * //Valor que oscila entre 0 y 1 en función de lo alto que esté el avión. Así cuando está un poco alto ya no hay sustentación
            Vector3.ClampMagnitude(Vector3.up * 0.5f * Mathf.Pow(rb.velocity.magnitude, 1) * sustentationSurface * sustenationK, maxSustentation); //Sustentation. Velocidad que empuja  al avión hacia arriba para vencer a la gravedad


        Debug.Log(sustentationVec.magnitude);
        //Si no estoy en el suelo, y tengo una velocidad mínima, la sustentación se fuerza a tener un valor mínimo. 
        if (!OnGround && rb.velocity.magnitude > velToDir / 2)
        {
            sustentationVec = sustentationVec.sqrMagnitude > (Vector3.up * minSustentation).sqrMagnitude ? sustentationVec : Vector3.up * minSustentation; //Mantiene la sustentación en un valor mínimo si ya estamos voland. Así el avión cae más suave si se apaga el motor
        }

        //Después, esta sustentación, ya forzada a tener un valor mínimo, se multiplica por el producto escalar de transform upcon vector3 up. Así, contra más para arriba mire el avión, menos sustentación tiene. Esto puede llegar a ser 0
        sustentationVec *= Mathf.Abs(Vector3.Dot(transform.up, Vector3.up)); 

        //Por último, si no estamos en el suelo y la sustentación final es menor que un porcentaje (stallMin) de la sustentación mínima, forzamos la sustentación a un mínimo. Así se puede llegar a ascender en vertical
        if (!onGround && sustentationVec.sqrMagnitude < (Vector3.up * minSustentation * stallMinSustentation).sqrMagnitude)
            sustentationVec = Vector3.up * minSustentation * stallMinSustentation;
        #endregion

        #region Orientation
        float currentVel = rb.velocity.magnitude;


       // maniobrability = 1 - thrustVec.sqrMagnitude*20;


        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))//if (Input.GetAxis("Horizontal") != 0)
        {
            yawPow = Mathf.Clamp(yawPow * 1 + yawRotRate * maniobrability * Time.deltaTime * Input.GetAxis("Horizontal"), -maxYaw, maxYaw);
        }
        else
        {
            if (yawPow < 0.01 && yawPow > -0.01)
                yawPow = 0;
            else
                yawPow = Mathf.Clamp(yawPow - (yawRecoverRate* Mathf.Sign(yawPow) * Time.deltaTime), -maxYaw, maxYaw);
        }
        if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow)))//if (Input.GetAxis("Vertical") != 0)
        {
            pitchPow = Mathf.Clamp(pitchPow * 1 + pitchRotRate * maniobrability * Time.deltaTime * Input.GetAxis("Vertical"), -maxPitch, maxPitch);
        }
        else
        {
            if (pitchPow < 0.01 && pitchPow > -0.01)
                pitchPow = 0;
            else
                pitchPow = Mathf.Clamp(pitchPow - (pitchRecoverRate * Mathf.Sign(pitchPow) * Time.deltaTime), -maxPitch, maxPitch);
        }
        if (Input.GetKey(KeyCode.Q))
            rollPow = Mathf.Clamp(rollPow + rollRotRate * maniobrability * Time.deltaTime, -maxRoll, maxRoll);
        else if(Input.GetKey(KeyCode.E))
            rollPow = Mathf.Clamp(rollPow - rollRotRate * maniobrability * Time.deltaTime, -maxRoll, maxRoll);
        else
        {
            if (rollPow < 0.01 && rollPow > -0.01)
                rollPow = 0;
            else
                rollPow = Mathf.Clamp(rollPow - (rollRecoverState * Mathf.Sign(rollPow) * Time.deltaTime), -1, 1);
        }

        #endregion

        //Debug.Log(sustentationVec.magnitude);
        rb.velocity += thrustVec + sustentationVec + brakeVec;
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVel);
       


        if (pitchPow != 0 || yawPow != 0 || rollPow != 0)
            transform.rotation *= Quaternion.Slerp(Quaternion.identity, Quaternion.Euler(new Vector3(pitchPow, yawPow, rollPow)), rb.velocity.magnitude / maxVel);

        float aux = 0.1f;
        rb.velocity = (transform.forward * rb.velocity.magnitude) * aux + rb.velocity * (1 - aux);
        if (rb.velocity.magnitude > 1.5)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(rb.velocity.normalized, transform.up), aux);

        //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(directionVec), dirSlerpAlpha), Mathf.Clamp01(rb.velocity.magnitude / velToDir));
        
    }
}
