﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Camera_Movement : MonoBehaviour
{
    public Transform lookAt;
    private Rigidbody lookAtRB;
    private AirCraft_Movement lookAtAM;
    ModeChanger characterMode;

    [Header("Aircraft mode")]
    [Space(5)]
    public float minDist=15;
    public float maxDist=35;
    public float angle = 70;
    [Range(0, 80)]
    public float hardness = 0.05f;


    [Header("Sphere mode")]
    [Space(5)]
    public float distanceS = 5;
    public Vector2 pitchMinMax = Vector2.zero;
    public float sensitivity = 1.0f;
    public float slerpAlpha = 0.25f;
    [Range(0, 80)]
    public float hardnessMov = 0.05f;
    private Vector2 offset = Vector2.zero;
    private Vector3 vel = Vector3.zero;
    private Quaternion currentRot;

       
    private void Start()
    {
        transform.position = lookAt.position;
        transform.rotation = lookAt.rotation;

        characterMode = lookAt.gameObject.GetComponent<ModeChanger>();
        lookAtRB = lookAt.gameObject.GetComponent<Rigidbody>();
        lookAtAM = lookAt.gameObject.GetComponent<AirCraft_Movement>();
    }

    private void Update()
    {
        if (characterMode.getMode() == global::characterMode.aircraft)
        {
            offset = new Vector2(transform.eulerAngles.y, transform.eulerAngles.x);
        }
        else
        {
            offset.x += Input.GetAxis("Mouse X") * sensitivity;
            offset.y += Input.GetAxis("Mouse Y") * sensitivity;
            offset.y = Mathf.Clamp(offset.y, pitchMinMax.x, pitchMinMax.y);
        }

        
    }

    private void LateUpdate()
    {
        currentRot = Quaternion.Slerp(currentRot, Quaternion.Euler(new Vector3(offset.y, offset.x, 0)), slerpAlpha*Time.deltaTime);
        if (characterMode.getMode() == global::characterMode.aircraft)
            updateCamAircraft();
        else
            updateCamSphere();
    }

    void updateCamAircraft()
    {
        Vector3 desiredPos = (Quaternion.AngleAxis(angle, lookAt.right) * -lookAt.forward) * Mathf.Lerp(minDist, maxDist, lookAtRB.velocity.magnitude/lookAtAM.maxVel);
        Vector3 smoothedPos = Vector3.Lerp(transform.position, lookAt.position + desiredPos, hardness*Time.deltaTime);
        transform.position = smoothedPos;

        Quaternion desiredRot = Quaternion.LookRotation((lookAt.position - transform.position).normalized);
        Quaternion smoothedRot = Quaternion.Slerp(transform.rotation, desiredRot, hardness*Time.deltaTime);
        transform.rotation = smoothedRot;

        
        //currentRot = lookAt.rotation;
        //offset.x = transform.eulerAngles.y;
        //offset.y = transform.eulerAngles.x;
    }

    void updateCamSphere()
    {
        //currentRot = Vector3.SmoothDamp(currentRot, new Vector3(offset.y, offset.x, 0), ref vel, smoothTime);
        
        transform.rotation = currentRot;
        transform.position = Vector3.Lerp(transform.position, lookAt.position + transform.forward * -distanceS, hardnessMov*Time.deltaTime);
    }

}