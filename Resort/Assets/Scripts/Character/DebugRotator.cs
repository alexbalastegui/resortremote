﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugRotator : MonoBehaviour
{
    public float timeToRot = 0.2f;
    [Range(0.1f, 1)]
    public float sensitivitiy = 1;
    private Vector3 vel = Vector3.zero;
    private float localPitch, localYaw, localRoll = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        localPitch += Input.GetAxis("Vertical")*sensitivitiy;
        localYaw += Input.GetAxis("Horizontal")*sensitivitiy;
        if (Input.GetKey(KeyCode.Q))
            localRoll += sensitivitiy;
        if (Input.GetKey(KeyCode.E))
            localRoll -= sensitivitiy;

        transform.rotation = Quaternion.Euler( new Vector3(localPitch, localYaw, localRoll));
    }
}
