﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomGridBrush]
public class GroundChecker : MonoBehaviour
{
    public AirCraft_Movement script;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        script.OnGround = true;

    }
    private void OnTriggerExit(Collider other)
    {
        script.OnGround = false;
    }
}
