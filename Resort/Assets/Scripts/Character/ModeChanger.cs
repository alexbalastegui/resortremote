﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum characterMode
{
    aircraft,
    sphere
}

public class ModeChanger : MonoBehaviour
{
    public float timeToTransform = 1f;
    private float timeToTransformEdit = 0;

    [SerializeField]
    private characterMode mode = characterMode.aircraft;

    AirCraft_Movement scriptAircraftMov;
    AirCraft_MoveParts scriptAircraftParts;
    Sphere_Movement scriptSphereMov;
    public GameObject aircraftGO;
    public GameObject sphereGO;
    private Rigidbody rb;

    bool canChange = true;


    private void Start()
    {
        scriptAircraftMov = GetComponent<AirCraft_Movement>();
        scriptAircraftParts = GetComponent<AirCraft_MoveParts>();
        scriptSphereMov = GetComponent<Sphere_Movement>();

        rb = GetComponent<Rigidbody>();

        setModeStart(mode);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            if (timeToTransformEdit >= timeToTransform && canChange)
            {
                if (mode == characterMode.aircraft)
                    setMode(characterMode.sphere);
                else
                    setMode(characterMode.aircraft);
                canChange = false;
            }
            else
                timeToTransformEdit += Time.deltaTime;
        }
        if (Input.GetKeyUp(KeyCode.Z)){
            canChange = true;
            timeToTransformEdit = 0;
        }
           
    }

    public void setMode(characterMode _mode, bool forceChange=false)
    {
        if (mode != _mode || forceChange)
        {
            mode = _mode;
            switch (mode)
            {
                case characterMode.aircraft:
                    transform.rotation = Quaternion.LookRotation(scriptSphereMov.LastForwardDir);
                    rb.velocity = rb.angularVelocity = Vector3.zero;
                    rb.mass = 5;

                    scriptAircraftMov.enabled = true;
                    scriptAircraftParts.enabled = true;
                    aircraftGO.SetActive(true);

                    scriptSphereMov.enabled = false;
                    sphereGO.SetActive(false);
                    break;
                case characterMode.sphere:
                    scriptAircraftMov.acc = 0;
                    rb.mass = 35;

                    scriptAircraftMov.enabled = false;
                    scriptAircraftParts.enabled = false;
                    aircraftGO.SetActive(false);

                    scriptSphereMov.enabled = true;
                    sphereGO.SetActive(true);
                    break;
            }
        }
    }
    public void setModeStart(characterMode _mode)
    {
        
            mode = _mode;
            switch (mode)
            {
                case characterMode.aircraft:
                    scriptAircraftMov.enabled = true;
                    scriptAircraftParts.enabled = true;
                    aircraftGO.SetActive(true);
                    rb.mass = 5;

                    scriptSphereMov.enabled = false;
                    sphereGO.SetActive(false);
                    break;
                case characterMode.sphere:
                    scriptAircraftMov.enabled = false;
                    scriptAircraftParts.enabled = false;
                    aircraftGO.SetActive(false);
                    rb.mass = 35;

                    scriptSphereMov.enabled = true;
                    sphereGO.SetActive(true);
                    break;
            }
    }
    public characterMode getMode() => mode;
}
