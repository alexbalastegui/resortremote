﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere_Movement : MonoBehaviour
{
    private Rigidbody rb;
    private Vector3 dirForward;
    private Vector3 dirRight;
    private Vector3 lastForwardDir;

    [Header("Movement")]
    public float speedBoost = 1;
    public float maxSpeed = 10;
    public float jumpPow = 3;
    [SerializeField]
    private bool onGround = true;

    public Vector3 DirForward { get => dirForward; set => dirForward = value; }
    public Vector3 DirRight { get => dirRight; set => dirRight = value; }
    public Vector3 LastForwardDir { get => lastForwardDir;}
    public bool OnGround { get => onGround; set => onGround = value; }



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        lastForwardDir = transform.forward;
    }

    private void FixedUpdate()
    {
        dirForward = Camera.main.transform.forward;
        dirForward -= Vector3.up * Vector3.Dot(dirForward, Vector3.up);
        dirForward = dirForward.normalized;
        dirRight = Camera.main.transform.right;
        dirRight -= Vector3.up * Vector3.Dot(dirRight, Vector3.up);
        dirRight = dirRight.normalized;

        rb.angularVelocity = new Vector3(rb.velocity.z, 0, -rb.velocity.x);

        if (rb.velocity.sqrMagnitude >= 1.0f)
            lastForwardDir = rb.velocity.normalized;
    }

    // Update is called once per frame
    void Update()
    {
        if (OnGround)
        {
            if (Input.GetAxis("Vertical") != 0)
                rb.AddForce(dirForward * Input.GetAxis("Vertical") * speedBoost, ForceMode.Acceleration);
            //rb.AddTorque(Camera.main.transform.right * Input.GetAxis("Vertical")*speedBoost);
            if (Input.GetAxis("Horizontal") != 0)
                rb.AddForce(dirRight * Input.GetAxis("Horizontal") * speedBoost, ForceMode.Acceleration);
            //rb.AddTorque(Camera.main.transform.forward * -Input.GetAxis("Horizontal")*speedBoost);
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);

            if (Input.GetKeyDown(KeyCode.Space))
                rb.AddForce(Vector3.up * jumpPow, ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        OnGround = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        OnGround = false;
    }
}
