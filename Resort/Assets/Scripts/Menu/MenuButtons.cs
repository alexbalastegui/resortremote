﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public void PlayButton()
    {
        SceneManager.LoadScene("Game.Constant_Scene");
    }

    public void OptionsButton()
    {

    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
