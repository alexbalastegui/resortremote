﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ObjectSnapToFloor : MonoBehaviour
{
    public bool execOnUpdate = false;
    public float minScale=0.9f;
    public float maxScale=1.45f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (execOnUpdate)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, Random.Range(0, 360), transform.eulerAngles.z);
            float temp = Random.Range(minScale, maxScale);
            transform.localScale = new Vector3(temp, temp, temp);
            execOnUpdate = false;

        }
    }
}
