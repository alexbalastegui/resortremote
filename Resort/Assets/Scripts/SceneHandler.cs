﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public int ownScene = 2;
    private List<int> scenesToLoad;
    public bool playerIn = false;

    private void Start()
    {
        scenesToLoad = new List<int>();
        scenesToLoad.Clear();
        scenesToLoad.Add(ownScene);

        if (ownScene <= 9 || ownScene >= 58 || (ownScene - 9) % 8 == 0 || (ownScene - 10) % 8 == 0)
        {
            if(ownScene <= 9) //left col
            {
                scenesToLoad.Add((ownScene + 8)); //right
                if ((ownScene - 9) % 8 == 0)
                {
                    scenesToLoad.Add((ownScene - 1)); //down
                    scenesToLoad.Add((ownScene + 7));
                }
                else if ((ownScene - 10) % 8 == 0)
                {
                    scenesToLoad.Add((ownScene + 1)); //up
                    scenesToLoad.Add((ownScene + 9));
                }
                else
                {
                    scenesToLoad.Add((ownScene - 1)); //down
                    scenesToLoad.Add((ownScene + 7));
                    scenesToLoad.Add((ownScene + 1)); //up
                    scenesToLoad.Add((ownScene + 9));
                }
            }
            else if (ownScene >= 58) //right col
            {
                scenesToLoad.Add((ownScene - 8)); //left
                if ((ownScene - 9) % 8 == 0)
                {
                    scenesToLoad.Add((ownScene - 1)); //down
                    scenesToLoad.Add((ownScene + 9));
                }
                else if ((ownScene - 10) % 8 == 0)
                {
                    scenesToLoad.Add((ownScene + 1)); //up
                    scenesToLoad.Add((ownScene - 7));
                }
                else
                {
                    scenesToLoad.Add((ownScene - 1)); //down
                    scenesToLoad.Add((ownScene + 9));
                    scenesToLoad.Add((ownScene + 1)); //up
                    scenesToLoad.Add((ownScene - 7));
                }
            }
            else if((ownScene - 9) % 8 == 0) //upper row
            {
                scenesToLoad.Add((ownScene - 8)); //left
                scenesToLoad.Add((ownScene + 8)); //right
                scenesToLoad.Add((ownScene - 1)); //down
                scenesToLoad.Add((ownScene - 9));
                scenesToLoad.Add((ownScene + 7));
            }
            else if((ownScene - 10) % 8 == 0) //lower row
            {
                scenesToLoad.Add((ownScene - 8)); //left
                scenesToLoad.Add((ownScene + 8)); //right
                scenesToLoad.Add((ownScene + 1)); //up
                scenesToLoad.Add((ownScene - 7));
                scenesToLoad.Add((ownScene + 9));
            }
        }
        else
        {
            scenesToLoad.Add((ownScene + 1)); //up
            scenesToLoad.Add((ownScene - 1)); //down
            scenesToLoad.Add((ownScene + 8)); //right
            scenesToLoad.Add((ownScene - 8)); //left
            scenesToLoad.Add((ownScene - 9)); //diagonals
            scenesToLoad.Add((ownScene - 7));
            scenesToLoad.Add((ownScene + 9));
            scenesToLoad.Add((ownScene + 7));
        }

        if (playerIn)
        {
            StartCoroutine(delayedSceneManagement(0.5f));
            SceneManagement();
        }
        else
            SceneManager.UnloadSceneAsync(ownScene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !playerIn)
        {
            SceneManagement();
            Debug.Log("in");
        }
    }

    void SceneManagement()
    {
        playerIn = true;
        for (int i = 2; i <= 65; ++i) //Los índices de las escenas de mapa van del 2 al 17
        {
            if (scenesToLoad.Contains(i) || i==ownScene) //debería estar cargado
            {
                if (!SceneManager.GetSceneByBuildIndex(i).isLoaded) //no está cargado
                    SceneManager.LoadSceneAsync(i, LoadSceneMode.Additive);
            }
            else //no debería estar cargado
            {
                if (SceneManager.GetSceneByBuildIndex(i).isLoaded) //está cargado
                    SceneManager.UnloadSceneAsync(i, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && playerIn)
        {
            playerIn = false;

            Debug.Log("out");
        }
    }
    IEnumerator delayedSceneManagement(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        SceneManagement();
    }
}

